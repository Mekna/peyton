import React, { Component }              from 'react';
import { StyleSheet, Text, View, Alert, TouchableOpacity} from 'react-native';
import { Camera, Permissions, FileSystem } from 'expo';

export class CameraScreen extends Component {
    state = {
        hasCameraPermission: null,
        type: Camera.Constants.Type.back,
    };

    async componentWillMount() {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({ hasCameraPermission: status === 'granted' });
    }

    takePicture = () => {
        if (this.camera) {
            this.camera.takePictureAsync({ onPictureSaved: this.onPictureSaved });
        }
    };

    onPictureSaved = async photo => {
        await FileSystem.copyAsync({
            from: photo.uri,
            to: FileSystem.documentDirectory + 'photos/' + Date.now() +'.jpg',
        }).then((succ)=>console.log("Successfully moved pic : "+succ), (err)=>console.log("Could not move pic : "+err));
        this.setState({ newPhotos: true });
    };

    _handleBarCode = (code) => {
        this.camera.pausePreview();
        if (code.data.length !== 10) {
            Alert.alert('Code-barres', 'Le code barre scanné ('+code.type+', '+code.data+') n\'est pas valide', [
                {text: 'Réessayer', onPress: () => this.camera.resumePreview()}
            ], {cancelable: false});
        } else {
            this.props.navigation.navigate('Home', {tchekid: code.data});
        }
    };

    renderUI = () => {
        if (this.props.navigation.getParam('mode', '') === 'capture') {
            return (
                <View
                    style={{
                        flex: 1,
                        backgroundColor: 'transparent',
                        flexDirection: 'row',
                        alignItems: 'flex-end',
                        justifyContent: 'space-between'
                    }}>
                    <TouchableOpacity
                        style={{
                            flex: 0.2,
                            alignItems: 'center',
                        }}
                        onPress={() => {
                            this.setState({
                                type: this.state.type === Camera.Constants.Type.back
                                    ? Camera.Constants.Type.front
                                    : Camera.Constants.Type.back,
                            });
                        }}>
                        <Text
                            style={{ fontSize: 18, marginBottom: 10, color: 'white' }}>
                            {' '}Flip{' '}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{
                            flex: 0.2,
                            alignItems: 'center',
                        }}
                        onPress={() => {
                            this.takePicture();
                        }}>
                        <Text
                            style={{ fontSize: 18, marginBottom: 10, color: 'azure', fontWeight: 'bold' }}>
                            {' '}Capture{' '}
                        </Text>
                    </TouchableOpacity>
                </View>
            )
        } else if (this.props.navigation.getParam('mode', '') === 'scan') {
            return (
                <View
                    style={{
                        flex: 1,
                        backgroundColor: 'transparent',
                        flexDirection: 'row',
                        alignItems: 'flex-end',
                        justifyContent: 'space-between'
                    }}>

                </View>
            )
        }
    }

    render() {
        const { hasCameraPermission } = this.state;
        if (hasCameraPermission === null) {
            return <View />;
        } else if (hasCameraPermission === false) {
            return <Text>No access to camera</Text>;
        } else {
            return (
                <View style={{ flex: 1 }}>
                    <Camera ref={ref => {this.camera = ref}} style={{ flex: 1 }} type={this.state.type} onBarCodeRead={this._handleBarCode}>
                        {this.renderUI()}
                    </Camera>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start'
    }
});