import React from 'react';
import { Text, TextInput } from 'react-native';

export class MonoText extends React.Component {
  render() {
    return <Text {...this.props} style={[this.props.style, { fontFamily: 'space-mono' }]} />;
  }
}

export class MonoInput extends React.Component {
  render() {
    return <TextInput {...this.props} style={[this.props.style, { fontFamily: 'space-mono' }]} />;
  }
}
