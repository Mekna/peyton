import React, { Component }             from 'react';
import { StyleSheet, Text, View, Alert} from 'react-native';
import { BarCodeScanner, Permissions }                  from "expo";

export class BScannerScreen extends Component {
    state = {
        hasCameraPermission: null,
    };

    async componentWillMount() {
        const {status} = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({hasCameraPermission: status === 'granted'});
    }
    _handleBarCodeRead = ({ type, data }) => {
        Alert.alert('Barcode', 'Bar code with type ${type} and data ${data} has been scanned!');
    };

    render() {
        const { hasCameraPermission } = this.state;

        if (hasCameraPermission === null) {
            return <Text>Requesting for camera permission</Text>;
        } else if (hasCameraPermission === false) {
            return <Text>No access to camera</Text>;
        } else {
            return (
                <View style={{ flex: 1 }}>
                    <BarCodeScanner
                        onBarCodeRead={this._handleBarCodeRead}
                        style={StyleSheet.absoluteFill}
                    />
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start'
    }
});