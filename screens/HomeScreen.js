import React from 'react';
import {
    Image,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    TouchableNativeFeedback,
    View,
    AsyncStorage} from 'react-native';
import {NavigationEvents} from 'react-navigation';
import { WebBrowser } from 'expo';
import Button from 'apsl-react-native-button';
import { MonoText, MonoInput } from '../components/StyledText';
import Parse from 'parse/react-native';

export default class HomeScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            serverType: 'local',
            idInput: props.navigation.getParam('tchekid', '')
        };
        Parse.setAsyncStorage(AsyncStorage);
    }
    static navigationOptions = {
        header: null,
    };

    componentWillMount() {
        const focusSubscription = this.props.navigation.addListener(
            'didFocus',
            payload => {
                console.debug('didFocus', payload);
                this.setState({idInput: this.props.navigation.getParam('tchekid', '')});
            }
        );
    }

    componentDidMount() {
        Parse.initialize('t2JMzLOHQWeMKwZLMmWaNhfaX6ar11k1v1IqvXG9', 'BRKLatHH3LjRDKRt2l5kUmcdA2dxj4yYOECkFMnO');
        Parse.serverURL = 'https://parseapi.back4app.com/';
        // let user = new Parse.User();

        Parse.User.logIn('demo', 'tchek').then(
            (succ) => console.log("LOGIN SUCCESS", succ),
            (err) => console.log("LOGIN FAILED", err)
        );
    }

    componentWillUnmount() {
        // Remove the listener when you are done
        focusSubscription.remove();
    }

    _handleLearnMorePress = () => {
        WebBrowser.openBrowserAsync('https://docs.expo.io/versions/latest/guides/development-mode');
    };

    _handleNavigationDidFocus = (payload) => {
        console.log("Navigation : Homescreen did focus");
        this.setState({idInput: this.props.navigation.getParam('tchekid', '')});
    };

    renderServerType = () => {
        if (this.state.serverType === 'local') {
            return (
                <Text style={styles.grayPill}>LOCAL</Text>
            )
        } else if (this.state.serverType === 'remote') {
            return (
                <Text style={styles.bluePill}>REMOTE</Text>
            )
        }

    };

    render() {
        return (
            <View style={styles.container}>
                {/*<NavigationEvents*/}
                    {/*onWillFocus={payload => {console.log("will focus", payload); }}*/}
                    {/*onDidFocus={payload => this.setState({idInput: this.props.navigation.getParam('tchekid', '')})}*/}
                    {/*onWillBlur={payload => console.log('will blur',payload)}*/}
                    {/*onDidBlur={payload => console.log('did blur',payload)}*/}
                {/*/>*/}
                <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
                    {/*<View style={styles.welcomeContainer}>*/}
                        {/*<Image*/}
                            {/*source={*/}
                                {/*__DEV__*/}
                                    {/*? require('../assets/images/robot-dev.png')*/}
                                    {/*: require('../assets/images/robot-prod.png')*/}
                            {/*}*/}
                            {/*style={styles.welcomeImage}*/}
                        {/*/>*/}
                    {/*</View>*/}

                    <View style={styles.getStartedContainer}>

                        <Text style={styles.getStartedText}>ID Tchek</Text>

                        <View style={[styles.codeHighlightContainer, styles.homeScreenFilename]}>
                            <MonoInput style={[styles.codeHighlightText, {width: 102, textAlign: 'center', textAlignVertical: 'top'}]} maxLength={10} numberOfLines={1} onChangeText={(value) => this.setState({ idInput: value })} placeholder={'4gTe01DfM7'} value={this.state.idInput}/>
                        </View>
                    </View>

                    <View style={styles.helpContainer}>
                        <Button style={styles.blueButton} textStyle={{fontWeight: 'bold', color: 'white'}} onPress={() => this.props.navigation.navigate('Camera', {mode: 'scan'})}>
                            Scan
                        </Button>
                    </View>

                    <View style={styles.dataContainer}>
                        <Text style={styles.getStartedText}>Tchek info & data.</Text>
                    </View>
                </ScrollView>

                <View style={styles.tabBarInfoContainer}>
                    <Text style={styles.tabBarInfoText}>Vous êtes connecté sur le serveur :</Text>
                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'baseline', justifyContent: 'space-between'}}>
                        {this.renderServerType()}
                        <View style={[styles.codeHighlightContainer, styles.navigationFilename]}>
                            <MonoText style={styles.codeHighlightText}>192.168.1.1</MonoText>
                        </View>
                    </View>
                </View>
            </View>
        );
    }

    _maybeRenderDevelopmentModeWarning() {
        if (__DEV__) {
            const learnMoreButton = (
                <Text onPress={this._handleLearnMorePress} style={styles.helpLinkText}>
                    Learn more
                </Text>
            );

            return (
                <Text style={styles.developmentModeText}>
                    Development mode is enabled, your app will be slower but you can use useful development
                    tools. {learnMoreButton}
                </Text>
            );
        } else {
            return (
                <Text style={styles.developmentModeText}>
                    You are not in development mode, your app will run at full speed.
                </Text>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    developmentModeText: {
        marginBottom: 20,
        color: 'rgba(0,0,0,0.4)',
        fontSize: 14,
        lineHeight: 19,
        textAlign: 'center',
    },
    contentContainer: {
        paddingTop: 30,
    },
    welcomeContainer: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 20,
    },
    welcomeImage: {
        width: 100,
        height: 80,
        resizeMode: 'contain',
        marginTop: 3,
        marginLeft: -10,
    },
    getStartedContainer: {
        alignItems: 'center',
        marginHorizontal: 50,
    },
    homeScreenFilename: {
        marginVertical: 7,
    },
    codeHighlightText: {
        color: 'rgba(96,100,109, 0.8)',
    },
    codeHighlightContainer: {
        marginLeft: 8,
        backgroundColor: 'rgba(0,0,0,0.05)',
        borderRadius: 3,
        paddingHorizontal: 4,
    },
    getStartedText: {
        fontSize: 17,
        color: 'rgba(96,100,109, 1)',
        lineHeight: 24,
        textAlign: 'center',
    },
    tabBarInfoContainer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        ...Platform.select({
            ios: {
                shadowColor: 'black',
                shadowOffset: { height: -3 },
                shadowOpacity: 0.1,
                shadowRadius: 3,
            },
            android: {
                elevation: 20,
            },
        }),
        alignItems: 'center',
        backgroundColor: '#fbfbfb',
        paddingVertical: 20,
    },
    tabBarInfoText: {
        fontSize: 17,
        color: 'rgba(96,100,109, 1)',
        textAlign: 'center',
    },
    navigationFilename: {
        marginTop: 5,
    },
    helpContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 15,
        paddingHorizontal: 8,
        justifyContent: 'space-evenly'
    },
    helpLink: {
        paddingVertical: 15,
    },
    helpLinkText: {
        fontSize: 14,
        color: '#2e78b7',
    },
    blueButton: {
        backgroundColor: '#328dd3',
        borderColor: '#358cd2',
        borderWidth: 2,
        flex: 0.45,
        borderRadius: 16,
    },
    dataContainer: {
        flex: 1,
        padding: 18,
        elevation: 12,
        backgroundColor: '#fbfbfb'
    },
    grayPill: {
        color: '#a5a5a5',
        fontSize: 8,
        backgroundColor: '#d9f2f4',
        borderRadius: 4,
        padding: 4
    },
    bluePill: {
        color: '#a5a5a5',
        fontSize: 8,
        backgroundColor: '#cfcff4',
        borderRadius: 4,
        padding: 4
    }
});
